/* SPDX-License-Identifier: MIT */
/*
 * Copyright © 2023 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include "config.h"

#include <errno.h>
#include <stdbool.h>

#include "util-bits.h"
#include "util-macros.h"
#include "util-mem.h"
#include "util-io.h"
#include "util-strings.h"
#include "util-version.h"

#include "libei-private.h"
#include "ei-proto.h"

static void
ei_connection_destroy(struct ei_connection *connection)
{
	struct ei *ei = ei_connection_get_context(connection);
	ei_unregister_object(ei, &connection->proto_object);

	struct ei_callback *cb;
	list_for_each_safe(cb, &connection->pending_callbacks, link) {
		list_remove(&cb->link);
		free(ei_callback_get_user_data(cb));
		ei_callback_unref(cb);
	}
}

OBJECT_IMPLEMENT_REF(ei_connection);
OBJECT_IMPLEMENT_UNREF_CLEANUP(ei_connection);

static
OBJECT_IMPLEMENT_CREATE(ei_connection);
static
OBJECT_IMPLEMENT_PARENT(ei_connection, ei);
OBJECT_IMPLEMENT_GETTER_AS_REF(ei_connection, proto_object, const struct brei_object*);

uint32_t
ei_connection_get_version(struct ei_connection *connection)
{
	return connection->proto_object.version;
}

object_id_t
ei_connection_get_id(struct ei_connection *connection)
{
	return connection->proto_object.id;
}

struct ei*
ei_connection_get_context(struct ei_connection *connection)
{
	assert(connection);
	return ei_connection_parent(connection);
}

const struct ei_connection_interface *
ei_connection_get_interface(struct ei_connection *connection) {
	struct ei *ei = ei_connection_parent(connection);
	return ei_get_interface(ei);
}

struct ei_connection *
ei_connection_new(struct ei *ei, object_id_t id, uint32_t version)
{
	struct ei_connection *connection = ei_connection_create(&ei->object);

	connection->proto_object.id = id;
	connection->proto_object.implementation = connection;
	connection->proto_object.interface = &ei_connection_proto_interface;
	connection->proto_object.version = version;
	ei_register_object(ei, &connection->proto_object);

	list_init(&connection->pending_callbacks);

	return connection; /* ref owned by caller */
}

struct callback_user_data {
	ei_connection_sync_callback_t cb;
	void *user_data;
};

static void
sync_callback(struct ei_callback *callback, void *callback_data, uint64_t proto_data)
{
	struct ei_connection *connection = callback_data;

	_cleanup_free_ struct callback_user_data *data = ei_callback_get_user_data(callback);
	if (data->cb)
		data->cb(connection, data->user_data);

	/* remove from pending callbacks */
	list_remove(&callback->link);
	ei_callback_unref(callback);
}

void
ei_connection_sync(struct ei_connection *connection, ei_connection_sync_callback_t cb, void *user_data)
{
	struct ei *ei = ei_connection_get_context(connection);

	/* This is double-wrapped because we only use this for debugging purposes for
	 * now. The actual callback calls sync_callback with our connection,
	 * then we extract the user_data on the object and call into the
	 * cb supplied to this function.
	 */
	struct ei_callback *callback = ei_callback_new(ei, sync_callback, connection);
	struct callback_user_data *data = xalloc(sizeof *data);
	data->cb = cb;
	data->user_data = user_data;
	ei_callback_set_user_data(callback, data);
	list_append(&connection->pending_callbacks, &callback->link);

	ei_connection_request_sync(connection, ei_callback_get_id(callback), ei_callback_get_version(callback));
}
